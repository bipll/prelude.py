import os
import sys
import time
import logging
from functools import wraps, partial
from itertools import chain


def fgetattr(obj, attr: str):
    """ Returns obj's attr or None, if obj is None. """
    return getattr(obj, attr) if obj is not None else None




class LockGuard:
    """ Turns any pair of callables/any thread.Lock-like object into a context manager, to be used with with operator. """
    def __init__(self, mutex=None, lock: callable=None, unlock: callable=None):
        """
        :param mutex:   an object with nullary methods acquire(), called on entering with context, and release(), called on exiting the context, when lock or unlock is None
        :param lock:    called on entering the with operator (default: mutex.acquire)
        :param unlock:  called on exiting the with operator (default: mutex.release)
        """
        self._lock = lock or fgetattr(mutex, 'acquire')
        if not callable(self._lock):
            raise RuntimeError("LockGuard: lock is not callable")
        self._unlock = unlock or fgetattr(mutex, 'release')
        if not callable(self._unlock):
            raise RuntimeError("LockGuard: unlock is not callable")

    def __enter__(self):
        self._lock()

    def __exit__(self, *args):
        self._unlock()




if __name__ == '__main__':
    import pytest

    class Mutex:
        def __init__(self):
            self.acq, self.rel = 0, 0
        def acquire(self):
            self.acq += 1
        def release(self):
            self.rel += 1

    m = Mutex()
    assert (m.acq, m.rel) == (0, 0)
    n = 0
    with LockGuard(m):
        n += 1
    assert (m.acq, m.rel, n) == (1, 1, 1)

    def acq():
        global m
        m = 'Hi!'
    def rel():
        global n
        n += 'lo.'

    with LockGuard(lock=acq, unlock=rel):
        n = 'But '
    assert (m, n) == ('Hi!', 'But lo.')

    m = Mutex()
    assert (m.acq, m.rel) == (0, 0)
    n = 0
    try:
        with LockGuard(m):
            n += 1
            raise RuntimeError("Hi!")
    except RuntimeError as e:
        exc = str(e)
    assert (m.acq, m.rel, n) == (1, 1, 1)
    assert str(exc) == 'Hi!'
